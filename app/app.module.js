(function () {
    'use strict';

    angular.module('airfile', [
        'airfile.common',
        'airfile.landing',
        'airfile.history',
        'airfile.transfer',
        'airfile.account'
    ]);
})();
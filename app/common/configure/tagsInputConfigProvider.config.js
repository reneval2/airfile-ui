(function () {
    'use strict';

    tagsInputConfig.$inject = ["tagsInputConfigProvider"];
    angular
        .module('airfile.common')
        .config(tagsInputConfig);

    /* @ngInject */
    function tagsInputConfig(tagsInputConfigProvider) {
        tagsInputConfigProvider.setActiveInterpolation('tagsInput', { placeholder: true });
    }

})();
(function () {
    'use strict';

    configureLocalStorager.$inject = ["localStorageServiceProvider"];
    angular
        .module('airfile.common')
        .config(configureLocalStorager);

        function configureLocalStorager(localStorageServiceProvider) {
            localStorageServiceProvider
                .setStorageType('localStorage');
        };
})();
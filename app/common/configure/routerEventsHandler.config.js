(function () {
    'use strict';

    routeEventsHandler.$inject = ["$rootScope"];
    angular
        .module('airfile.common')
        .run(routeEventsHandler);

    /* @ngInject */
    function routeEventsHandler($rootScope) {
        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            $rootScope.title = toState.title || '';
        });
    }
})();
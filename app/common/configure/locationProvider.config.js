(function () {
    'use strict';

    configureLocationProvider.$inject = ["$locationProvider"];
    angular
        .module('airfile.common')
        .config(configureLocationProvider);

    /* @ngInject */
    function configureLocationProvider($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }
})();
(function () {
    'use strict';

    configureCompileProvider.$inject = ["$compileProvider"];
    angular
        .module('airfile.common')
        .config(configureCompileProvider);

    /* @ngInject */
    function configureCompileProvider($compileProvider) {
        // Disable various debug runtime information in the compiler
        // such as adding binding information and a reference to the current scope on to DOM elements
        $compileProvider.debugInfoEnabled(false);
    }
})();
(function () {
    'use strict';

    configureUnmatchedUrl.$inject = ["$urlRouterProvider"];
    angular
        .module('airfile.common')
        .config(['$urlRouterProvider', configureUnmatchedUrl]);

    /* @ngInject */
    function configureUnmatchedUrl($urlRouterProvider) {
        // For any unmatched url, redirect to the default url
        $urlRouterProvider.otherwise( function($injector) {
            var $state = $injector.get("$state");
            $state.go("transfer");
        });
    }
})();
(function () {
    'use strict';

    angular
        .module('airfile.common', [
            'airfile.blocks',
            'ui.router',
            'ui.bootstrap',
            'ngAnimate',
            'ngFileUpload',
            'LocalStorageModule',
            'ngTagsInput',

        ]);
})();
(function () {
    'use strict';

    angular
        .module('airfile.common')
        .directive('slideToggle', slideable);

    /* @ngInject */
    function slideable() {
        var directive = {
            restrict: 'A',
            link: link
        };

        function link(scope, element, attrs){
            var target = document.querySelector(attrs.slideToggle);
            var content;
            if (target){
                content = target.querySelector('.slideable_content');
            }

            attrs.expanded = false;
            element.bind('click', function() {
                target = target || document.querySelector(attrs.slideToggle);
                var content = content || target.querySelector('.slideable_content');
                if(!attrs.expanded) {
                    content.style.border = '1px solid rgba(0,0,0,0)';
                    var y = content.clientHeight;
                    content.style.border = 0;
                    target.style.height = y + 'px';
                } else {
                    target.style.height = '0px';
                }
                attrs.expanded = !attrs.expanded;
            });
        }

        return directive;
    }
})();
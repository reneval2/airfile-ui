(function () {
    'use strict';

    angular
        .module('airfile.common')
        .directive('afEnter', afEnter);

    /* @ngInject */
    function afEnter() {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.afEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    };
})();
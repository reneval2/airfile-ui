(function () {
    'use strict';

    commonService.$inject = ["$state"];
    angular
        .module('airfile.common')
        .factory('commonService', commonService);

    /* @ngInject */
    function commonService($state) {
        var service = {};
        service.gotoState = gotoState;


        function gotoState(state, params) {
            $state.go(state, params);
        }

        return service;
    }
})();
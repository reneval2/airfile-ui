(function () {
    'use strict';

    messageService.$inject = ["$q", "$timeout"];
    angular
        .module('airfile.common')
        .factory('messageService', messageService);

    /* @ngInject */
    function messageService($q, $timeout) {
        var service = {};
        service.sendFeedback = sendFeedback;
        service.applyForDiscount = applyForDiscount;

        function sendFeedback() {
            // TODO here goes server side method exposed via api to accept feedback message
            return $timeout(function () {
                var deferred = $q.defer();
                deferred.resolve(true);
                return deferred.promise;
            }, 1000);
        }

        function applyForDiscount(email) {
            // TODO here goes server side method exposed via api to accept feedback message
            return $timeout(function () {
                var deferred = $q.defer();
                deferred.resolve(true);
                return deferred.promise;
            }, 1000);
        }

        return service;
    }
})();
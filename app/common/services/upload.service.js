(function () {
    'use strict';
    uploadService.$inject = ["$q"];
    angular
        .module('airfile.common')
        .factory('uploadService', uploadService);

    /* @ngInject */
    function uploadService($q) {
        var service = {};
        service.upload = upload;
        /*Upload.upload({
            url: 'upload/url',
            data: {file: file, 'username': $scope.username}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });*/

        function upload() {
            var deferred = $q.defer();
            deferred.resolve('/data/img/test-bg.jpg');
            return deferred.promise;
        }

        return service;
    }
})();
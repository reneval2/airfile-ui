(function () {
    'use strict';

    userService.$inject = ["$q"];
    angular
        .module('airfile.common')
        .factory('userService', userService);

    /* @ngInject */
    function userService($q) {
        var test = 0;
        var service = {};
        var userEmails = [];
        service.checkPro = checkPro;
        service.setEmails = setEmails;
        service.getEmails = getEmails;

        function setEmails(_emails){
            userEmails =  _emails.slice(0,0);
        }

        function getEmails(){
            return userEmails;
        }

        function checkPro(){
            var deferred = $q.defer();

            test++;
            deferred.resolve(test % 2 !== 1);
            return deferred.promise;
        }

        return service;
    }
})();
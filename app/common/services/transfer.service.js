(function () {
    'use strict';

    transferService.$inject = ["$q", "$timeout"];
    angular
        .module('airfile.common')
        .factory('transferService', transferService);

    /* @ngInject */
    function transferService($q, $timeout) {
        var service = {};
        service.getFilesByTransferId = getFilesByTransferId;
        service.getDownloadLink = getDownloadLink;
        service.loadSharingOptions = loadSharingOptions;
        service.loadQRCode = loadQRCode;

        function getFilesByTransferId(transferId) {
            var deferred = $q.defer();
            var files = [{name: 'SomeTransferedFile.png', size: 3547767}, {
                name: 'SomeTransferedFile2.png',
                size: 7786321
            }];
            deferred.resolve(files);
            return deferred.promise;
        }

        function getDownloadLink(someParams) {
            // here goes server side query
            return $timeout(function () {
                var id = Math.random().toString(36).substring(7, 14);
                var link = 'airfile.co/' + id;
                var deferred = $q.defer();
                deferred.resolve(link);
                return deferred.promise;
            }, 1000);
        }

        function loadSharingOptions(){
            // here goes server side query
            return $timeout(function () {
                var deferred = $q.defer();
                deferred.resolve(true);
                return deferred.promise;
            }, 1000);
        }

        function loadQRCode(){
            // here goes server side query
            return $timeout(function () {
                var deferred = $q.defer();
                deferred.resolve(true);
                return deferred.promise;
            }, 1000);
        }

        return service;
    }
})();
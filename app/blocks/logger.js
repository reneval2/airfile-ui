/*global angular*/

(function () {
    'use strict';

    logger.$inject = ["$log", "$injector"];
    angular
        .module('airfile.blocks')
        .factory('logger', logger);

    /* @ngInject */
    function logger($log, $injector) {
        var logLevel = {
            Fatal: 'Fatal',
            Error: 'Error',
            Warn: 'Warn',
            Info: 'Info',
            Debug: 'Debug',
            Trace: 'Trace'
        };

        function error(message, data, title) {
            $log.error('Error: ' + message, data);
        }

        function info(message, data, title) {
            $log.info('Info: ' + message, data);
        }

        function success(message, data, title) {
            $log.info('Success: ' + message, data);
        }

        function warning(message, data, title) {
            $log.warn('Warning: ' + message, data);
        }

        function exception(data) {
            var exceptionLogger = $injector.get('exceptionLogger'),
                $window = $injector.get('$window');

            data = angular.extend({}, data, {
                absUrl: $window.location.href,
                userAgent: $window.navigator.userAgent
            });

            // Save the error message on the server
            exceptionLogger.save({
                level: logLevel.Error,
                message: JSON.stringify(data)
            });
        }

        return {
            logLevel: logLevel,
            error: error,
            info: info,
            success: success,
            warning: warning,
            exception: exception
        };
    }
}());
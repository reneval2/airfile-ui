(function () {
    'use strict';

    angular
        .module('airfile.blocks')
        .factory('exceptionLogger', exceptionLogger);

    /* @ngInject */
    function exceptionLogger() {
        var service = {};
        service.save = save;

        function save(){
            // stub
        }

        return service;
    }
})();
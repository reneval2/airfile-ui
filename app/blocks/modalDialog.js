(function () {
    'use strict';

    modalDialog.$inject = ["$uibModal"];
    angular
        .module('airfile.blocks')
        .factory('modalDialog', modalDialog);

    /* @ngInject */
    function modalDialog($uibModal) {
        /* @ngInject */
        ModalInstance.$inject = ["$uibModalInstance", "options"];
        function ModalInstance($uibModalInstance, options) {
            var vm = this;
            vm.emails = (options.resolve && options.resolve.emails) ||[];
            vm.currentEmail = "";
            vm.title = options.title || '';
            vm.message = options.message || '';
            vm.okText = options.okText || 'OK';
            vm.cancelText = options.cancelText || 'Cancel';
            vm.showHeader = angular.isDefined(options.title);
            vm.addEmail = addEmail;
            vm.deleteEmail = deleteEmail;
            vm.ok = ok;
            vm.cancel = cancel;

            function ok(result) {
                $uibModalInstance.close(result);
            }

            function cancel() {
                $uibModalInstance.dismiss('cancel');
            }

            function addEmail(email) {
                if (!vm.addMailForm.$valid || !email) return;
                var i = findEmail(email);
                if (i < 0) {
                    vm.emails.push(email);
                    vm.currentEmail = "";
                }
            }

            function deleteEmail(email) {
                var i = findEmail(email);
                if (i >= 0) {
                    vm.emails.splice(i, 1);
                }
            }

            function findEmail(email) {
                return _.findIndex(vm.emails, function (o) {
                    return o == email;
                });
            }
        }

        return {
            enjoyPro: enjoyProDialog,
            gift: giftDialog,
            setBackground: setBackgroundDialog,
            help: helpDialog,
            signIn: signInDialog,
            forgotPassword: forgotPasswordDialog,
            passwordSentConfirmation: passwordSentConfirmationDialog,
            deleteConfirmation: deleteConfirmationDialog,
            shareOptions: shareOptionsDialog,
            transferToMobile: transferToMobileDialog,
            getDownloadLink: getDownloadLinkDialog,
            changePassword: changePasswordDialog,
            contactUs: contactUsDialog,
            choosePrivateSpace: choosePrivateSpaceDialog,
            addRemoveUsers: addRemoveUsersDialog,
            keepBackground: keepBackgroundDialog
        };

        function dialog(options) {
            var modalOptions = {
                templateUrl: options.template,
                title: options.title || '',
                controller: ModalInstance,
                controllerAs: 'modal',
                keyboard: true,
                backdrop: 'static', //  modal window is not closed when clicking outside of the modal window
                resolve: {
                    options: function () {
                        return options;
                    }
                }
            };

            if (options.backdrop === 'remove') {
                delete modalOptions.backdrop;
            }

            return $uibModal.open(modalOptions).result;
        }

        function enjoyProDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-enjoy-pro.html';
            return dialog(options);
        }

        function giftDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-gift.html';
            return dialog(options);
        }

        function setBackgroundDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-set-background.html';
            return dialog(options);
        }

        function helpDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-help.html';
            return dialog(options);
        }

        function signInDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-sign-in.html';
            return dialog(options);
        }

        function forgotPasswordDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-forgot-password.html';
            return dialog(options);
        }

        function passwordSentConfirmationDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-password-sent.html';
            return dialog(options);
        }

        function deleteConfirmationDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-confirm-deletion.html';
            return dialog(options);
        }

        function shareOptionsDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-sharing-options.html';
            return dialog(options);
        }

        function transferToMobileDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-transfer-to-mobile.html';
            return dialog(options);
        }

        function getDownloadLinkDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-get-link.html';
            return dialog(options);
        }

        function changePasswordDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/account/air-file-change-password.html';
            return dialog(options);
        }

        function contactUsDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/account/air-file-contact-us-form.html';
            return dialog(options);
        }

        function choosePrivateSpaceDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/account/air-file-private-space.html';
            return dialog(options);
        }

        function addRemoveUsersDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/account/air-file-add-remove-users.html';
            return dialog(options);
        }

        function keepBackgroundDialog(options) {
            options = options || {};
            options.template = 'app/layout/templates/air-file-keep-background.html';
            return dialog(options);
        }
    }
})();

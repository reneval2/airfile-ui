(function () {
    'use strict';

    var config = {
        appErrorPrefix: '[AirFile] ', // Configure the exceptionHandler decorator
        version: '0.0.1'
    };

    angular
        .module('airfile.blocks')
        .constant('config', config);
})();
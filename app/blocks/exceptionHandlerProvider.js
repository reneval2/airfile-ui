(function () {
    'use strict';

    extendExceptionHandler.$inject = ["$delegate", "config", "logger"];
    exceptionHandler.$inject = ["$provide"];
    angular
        .module('airfile.blocks')
        .config(exceptionHandler);

    /* @ngInject */
    function extendExceptionHandler($delegate, config, logger) {
        var appErrorPrefix = config.appErrorPrefix;

        return function (exception, cause) {
            var errorData = {
                name: exception.name,
                message: exception.message,
                description: exception.description,
                stack: exception.stack,
                cause: cause
            };

            exception.message = appErrorPrefix + exception.message;
            $delegate(exception, cause);
            logger.error(appErrorPrefix + exception.message);
            logger.exception(errorData);
        };
    }

    // Configure by setting an optional string value for appErrorPrefix.
    // Accessible via config.appErrorPrefix (via config value).
    /* @ngInject */
    function exceptionHandler($provide) {
        $provide.decorator('$exceptionHandler', extendExceptionHandler);
    }
})();
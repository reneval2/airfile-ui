(function () {
    'use strict';

    configureStates.$inject = ["$stateProvider"];
    angular
        .module('airfile.account')
        .config(configureStates);

    /* @ngInject */
    function configureStates($stateProvider) {
        $stateProvider
            .state('account', {
                url: '/account',
                title: 'My account',
                templateUrl: 'app/account/account.html',
                controller: 'AccountController',
                controllerAs: 'vm'
            });
    }
})();
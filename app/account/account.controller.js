(function () {
    'use strict';

    AccountController.$inject = ["modalDialog", "userService"];
    angular
        .module('airfile.account')
        .controller('AccountController', AccountController);

    /* @ngInject */
    function AccountController(modalDialog, userService) {
        var vm = this;
        vm.emails = [];
        vm.changePassword = changePassword;
        vm.contactUs = contactUs;
        vm.choosePrivateSpace = choosePrivateSpace;
        vm.addRemoveUsers = addRemoveUsers;
        vm.changeBackground = changeBackground;

        activate();

        function activate() {
            vm.emails = userService.getEmails();
        }

        function changePassword() {
            modalDialog.changePassword();
        }

        function contactUs() {
            modalDialog.contactUs();
        }

        function choosePrivateSpace() {
            modalDialog.choosePrivateSpace();
        }

        function addRemoveUsers() {
            modalDialog.addRemoveUsers(
                {
                    resolve: {
                        emails: vm.emails
                    }
                }
            ).then(function (res) {
                    userService.setEmails(res);
                });
        }

        function enjoyPro() {
            modalDialog.enjoyPro().then(function () {
                modalDialog.gift();
            });
        }

        function changeBackground() {
            userService.checkPro().then(function (data) {
                if (!data) {
                    enjoyPro();
                    return;
                }

                modalDialog.setBackground();
            });
        }
    }
})();
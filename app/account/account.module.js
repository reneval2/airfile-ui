(function () {
    'use strict';

    angular
        .module('airfile.account', [
            'airfile.layout'
        ]);
})();
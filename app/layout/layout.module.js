(function () {
    'use strict';

    angular
        .module('airfile.layout', [
            'airfile.common'
        ]);
})();
(function () {
    'use strict';

    SignInController.$inject = ["modalDialog"];
    angular
        .module('airfile.layout')
        .controller('SignInController', SignInController);

    /* @ngInject */
    function SignInController(modalDialog) {
        var vm = this;
        vm.forgotPassword = forgotPassword;
        vm.enjoyPro = enjoyPro;

        function forgotPassword(){
            modalDialog.forgotPassword().then(function(email){
                modalDialog.passwordSentConfirmation();
            });
        }

        function enjoyPro(){
            modalDialog.enjoyPro().then(function () {
                modalDialog.gift();
            });
        }
    }
})();
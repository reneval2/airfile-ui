(function () {
    'use strict';

    ShareController.$inject = ["modalDialog"];
    angular
        .module('airfile.layout')
        .controller('ShareController', ShareController);

    /* @ngInject */
    function ShareController(modalDialog) {
        var vm = this;
        vm.getDownloadLink = getDownloadLink;

        function getDownloadLink(){
            modalDialog.getDownloadLink();
        }
    }
})();
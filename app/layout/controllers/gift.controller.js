(function () {
    'use strict';

    GiftController.$inject = ["messageService"];
    angular
        .module('airfile.layout')
        .controller('GiftController', GiftController);

    /* @ngInject */
    function GiftController(messageService) {
        var vm = this;
        vm.checkSendActive = checkSendActive;
        vm.sendDiscountEmail = sendDiscountEmail;

        activate();

        function checkSendActive(value) {
            return value.indexOf('@') > -1
        }

        function activate() {
            vm.discountEmail = '';
        }

        function sendDiscountEmail() {
            messageService.applyForDiscount(vm.discountEmail).then(function () {
            });
        }
    }
})();
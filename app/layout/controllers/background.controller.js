(function () {
    'use strict';

    BackgroundController.$inject = ["uploadService", "$q"];
    angular
        .module('airfile.layout')
        .controller('BackgroundController', BackgroundController);

    /* @ngInject */
    function BackgroundController(uploadService, $q) {
        var vm = this;
        vm.uploadImage = uploadImage;

        function uploadImage($file) {
            uploadService.upload($file).then(function (data) {
                var deferred = $q.defer();
                deferred.resolve(data);
                vm.imagePath = data;
                return deferred.promise;
            });
        }
    }
})();
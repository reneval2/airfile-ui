(function () {
    'use strict';

    ShellController.$inject = ["$state"];
    angular
        .module('airfile.layout')
        .controller('ShellController', ShellController);

    /* @ngInject */
    function ShellController($state) {
        var vm = this;
        vm.menuOpened = false;
        vm.toggleMenu = toggleMenu;
        vm.isMenuActive = isMenuActive;
        vm.background = {};

        function toggleMenu() {
            vm.menuOpened = !vm.menuOpened;
        }

        function isMenuActive(item) {
            return $state.current.name === item;
        }
    }
})();
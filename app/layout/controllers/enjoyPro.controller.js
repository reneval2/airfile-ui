(function () {
    'use strict';

    angular
        .module('airfile.layout')
        .controller('EnjoyProController', EnjoyProController);

    /* @ngInject */
    function EnjoyProController() {
        var vm = this;
        vm.selected = '';
        vm.options = ['$10 per month - with 200GB history', 'Select 1', 'Select 2', 'Select 3', 'Select 4'];
        vm.selectPro = selectPro;

        activate();

        function selectPro(option){
            vm.selected = option;
        }

        function activate(){
            vm.selected = vm.options[0];
        }
    }
})();
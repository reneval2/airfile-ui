(function () {
    'use strict';

    MenuController.$inject = ["modalDialog", "messageService"];
    angular
        .module('airfile.layout')
        .directive('airFileMenu', mainMenu);

    /* @ngInject */
    function mainMenu() {
        var directive = {
            restrict: 'EA',
            controller: MenuController,
            controllerAs: 'menu',
            templateUrl: 'app/layout/templates/air-file-menu.directive.html'
        };

        return directive;
    }

    /* @ngInject */
    function MenuController(modalDialog, messageService) {
        var vm = this;
        vm.enjoyPro = enjoyPro;
        vm.signIn = signIn;
        vm.sendFeedback = sendFeedback;

        activate();

        function activate() {
            vm.messageSending = false;
            vm.feedbackPlaceholder = 'Send Feedback';
        }

        function enjoyPro() {
            modalDialog.enjoyPro().then(function () {
                modalDialog.gift();
            });
        }

        function signIn() {
            modalDialog.signIn();
        }

        function sendFeedback() {
            vm.messageSending = true;
            messageService.sendFeedback().then(function () {
                vm.feedbackMessage = '';
                vm.feedbackPlaceholder = 'Sent';
                vm.messageSending = false;
            })
        }
    }
})();
(function () {
    'use strict';

    angular
        .module('airfile.layout')
        .directive('airFileHeaderMenu', headerMenu);

    /* @ngInject */
    function headerMenu() {
        var directive = {
            restrict: 'EA',
            link: link,
            templateUrl: 'app/layout/templates/air-file-header-menu.directive.html',
            transclude: true
        };

        return directive;

        function link(scope, element, attrs) {
            var touchMenu = $('.touch-menu'),
                menu = $('#menu'),
                airfile = $('#airfile'),
                menuLi = menu.find('ul li'),
                durAnimMenu = 150,
                flagMenu = true;

            touchMenu.click(function (e) {
                e.preventDefault();

                if (flagMenu) {
                    flagMenu = false;
                    if (!(menu.hasClass('active'))) {
                        menu.addClass('active').animate({'width': '200px'}, 300, function () {
                            animElemHorizontal(menuLi, durAnimMenu, 'top-down');
                        });
                    } else {
                        menu.removeClass('active');
                        animElemHorizontal(menuLi, durAnimMenu, 'bottom-up');
                    }
                }
            });

            airfile.find('.tabs-cont').click(function () {
                if (flagMenu) {
                    if (menu.hasClass('active')) {
                        menu.removeClass('active');
                        animElemHorizontal(menuLi, durAnimMenu, 'bottom-up');
                    }
                }
            });

            function animElemHorizontal(elem, dur, direction) {
                if (direction == 'top-down') {
                    elem.first().animate({'opacity': 1}, dur, function () {
                        if ($(this).next().length) {
                            animElemHorizontal($(this).next(), durAnimMenu, 'top-down');
                        } else {
                            flagMenu = true;
                        }
                    });
                } else if (direction == 'bottom-up') {
                    elem.last().animate({'opacity': 0}, dur, function () {
                        if ($(this).prev().length) {
                            animElemHorizontal($(this).prev(), dur, 'bottom-up');
                        } else {
                            flagMenu = true;
                            menu.animate({
                                'width': 0,
                            }, 300);
                        }
                    });
                }
            }
        }
    }
})();
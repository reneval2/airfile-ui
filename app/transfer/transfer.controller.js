(function () {
    'use strict';

    TransferController.$inject = ["modalDialog", "$timeout", "userService", "transferService", "TransferConstants", "$window", "localStorageService"];
    angular
        .module('airfile.transfer')
        .controller('TransferController', TransferController);

    /* @ngInject */
    function TransferController(modalDialog, $timeout, userService, transferService, TransferConstants, $window, localStorageService) {
        var vm = this;
        vm.transferTooltipEnter = transferTooltipEnter;
        vm.transferTooltipLeave = transferTooltipLeave;
        vm.enjoyPro = enjoyPro;
        vm.selectFunction = selectFunction;
        vm.selectBackground = selectBackground;
        vm.addFiles = addFiles;
        vm.removeFile = removeFile;
        vm.bytesToHumanFormat = bytesToHumanFormat;
        vm.toggleFileMenu = toggleFileMenu;
        vm.getHelp = getHelp;
        vm.copyLink = copyLink;
        vm.openShareLink = openShareLink;
        vm.sendMail = sendMail;
        vm.FILE_MAX_LENGTH = 32;
        vm.linkCopied = false;
        vm.email ="";

        activate();

        function enjoyPro() {
            modalDialog.enjoyPro().then(function () {
                modalDialog.gift().then(function(){

                }, revertImage);
            }, revertImage);
        }

        function transferTooltipEnter(item) {
            switch (item) {
                case 'link':
                    vm.title = 'Get download link';
                    break;
                case 'email':
                    vm.title = 'Transfer files by email';
                    break;
                case 'share':
                    vm.title = 'Sharing Options';
                    break;
                case 'mobile':
                    vm.title = 'Transfer to mobile';
                    break;
            }
        }

        function selectFunction(item) {
            if (vm.selectedFunction === item) {
                return;
            }

            vm.selectedFunction = item;

            switch (item) {
                case 'link':
                    getDownloadLink();
                    vm.socialLoaded = false;
                    vm.qrCodeLoaded = false;
                    break;
                case 'email':
                    vm.downloadLink = '';
                    vm.socialLoaded = false;
                    vm.qrCodeLoaded = false;
                    break;
                case 'mobile':
                    getDownloadLink();
                    vm.downloadLink = '';
                    vm.qrCodeLoaded = false;
                    loadQRCode();
                    break;
                case 'share':
                    loadSharingOptions();
                    vm.downloadLink = '';
                    vm.socialLoaded = false;
                    break;
            }
        }

        function transferTooltipLeave() {
            vm.title = vm.titleActive;
        }

        function activate() {
            vm.defaultImage = '../content/img/bg-transfer.jpg';
            vm.imagePath = vm.defaultImage;
            vm.qrCodeLoaded = false;
            vm.downloadLink = '';
            vm.socialLoaded = false;
            vm.title = 'Transfer files by email';
            vm.titleActive = vm.title;
            vm.files = [];
            vm.selectedFunction = 'email';
            vm.emails = [];
            vm.totalSize = 0;
            vm.email = localStorageService.get("email") ||"";

            $timeout(function () {
                vm.showTransferBox = true;
                $timeout(function () {
                    vm.showEnjoyProBox = true;
                }, 300)
            }, 200)
        }

        function selectBackground() {
            modalDialog.setBackground().then(function (imagePath) {
                vm.imagePath = imagePath;
                userService.checkPro().then(function (data) {
                    if (data) {
                        return;
                    }

                    $timeout(function () {
                        modalDialog.keepBackground().then(function () {
                            enjoyPro();
                        }, revertImage);
                    }, 2000);
                });
            });
        }

        function addFiles(files) {
            for (var index = 0; index < files.length; index++) {
                var file = {name: files[index].name, size: files[index].size};
                vm.files.push(file);
            }

            calculateTotal();
        }

        function bytesToHumanFormat(size, measure) {
            size *= 1.0;
            if (size < 1024 * 1024) {
                measure = 'Kb';
            }
            else if (size < 1024 * 1024 * 1024) {
                measure = 'Mb';
            }
            else {
                measure = 'Gb';
            }

            var pow = 0;
            switch (measure) {
                case 'Kb':
                    pow = 1;
                    break;
                case 'Mb':
                    pow = 2;
                    break;
                case 'Gb':
                    pow = 3;
                    break;
            }

            return Math.round(size / Math.pow(1024, pow) * 100) / 100 + ' ' + measure;
        }

        function toggleFileMenu(file) {
            file.opened = !file.opened;
        }

        function removeFile(index) {
            vm.files.splice(index, 1);
            calculateTotal();
        }

        function calculateTotal() {
            vm.totalSize = 0;

            for (var index = 0; index < vm.files.length; index++) {
                vm.totalSize += vm.files[index].size;
            }
        }

        function getHelp() {
            modalDialog.help();
        }
        function copyLink() {
            vm.linkCopied =true;
        }

        function getDownloadLink() {
           return transferService.getDownloadLink().then(function (link) {
                vm.downloadLink = link;
               vm.linkCopied = false;
            })
        }

        function loadSharingOptions() {
            transferService.loadSharingOptions().then(function (link) {
                vm.socialLoaded = true;
            })
        }

        function loadQRCode() {
            transferService.loadSharingOptions().then(function (link) {
                vm.qrCodeLoaded = true;
            });
        }

        function revertImage() {
            vm.imagePath = vm.defaultImage;
        }

        function sendMail () {
            vm.imagePath = vm.defaultImage;
            if(vm.email) {
                localStorageService.set("email",vm.email);
            }
        }

        function openShareLink(socialNetwork){
            var URL_pattern = TransferConstants.SOCIAL_SHARER_LINKS[socialNetwork];
            if(!URL_pattern){
                return;
            }

            getDownloadLink()
              .then(function(){
                  var link = URL_pattern.replace('%LINK%', encodeURI(vm.downloadLink));
                  $window.open(link,'_blank');
              })
        }
    }
})();
(function () {
    'use strict';
    angular
      .module('airfile.transfer')
        .constant('TransferConstants', {
          SOCIAL_SHARER_LINKS:{
              fb:"https://www.facebook.com/sharer/sharer.php?u=%LINK%",
              tw: "https://twitter.com/home?status=%LINK%",
              in:"https://www.linkedin.com/shareArticle?mini=true&url=%LINK%&title=%LINK%&summary=&source=",
              gl:"https://plus.google.com/share?url=%LINK%"
          }
      });
})();
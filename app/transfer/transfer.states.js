(function () {
    'use strict';

    resolveTransferReceived.$inject = ["$stateParams", "transferService", "commonService"];
    configureStates.$inject = ["$stateProvider"];
    angular
        .module('airfile.transfer')
        .config(configureStates);

    /* @ngInject */
    function configureStates($stateProvider) {
        $stateProvider
            .state('transfer', {
                url: '/transfer',
                title: 'Transfer',
                templateUrl: 'app/transfer/transfer.html',
                controller: 'TransferController',
                controllerAs: 'vm'
            })
            .state('transfer-received', {
                url: '/transfer/:id',
                title: 'Transfer Received',
                templateUrl: 'app/transfer/transfer-received.html',
                controller: 'TransferReceivedController',
                controllerAs: 'vm',
                resolve: {
                    transferredFiles: resolveTransferReceived
                }
            });
    }

    /* @ngInject */
    function resolveTransferReceived($stateParams, transferService, commonService){
        return transferService.getFilesByTransferId($stateParams.id).then(function(data){
            if (!data || data.length === 0){
                commonService.gotoState('transfer');
                return [];
            }

            return data;
        });
    }
})();
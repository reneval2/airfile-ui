(function () {
    'use strict';

    angular
        .module('airfile.transfer', [
            'airfile.layout',
            'monospaced.qrcode',
            'ngclipboard',
            'dibari.angular-ellipsis'
        ]);
})();
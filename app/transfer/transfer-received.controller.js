(function () {
    'use strict';

    TransferReceivedController.$inject = ["modalDialog", "$timeout", "userService", "transferredFiles"];
    angular
        .module('airfile.transfer')
        .controller('TransferReceivedController', TransferReceivedController);

    /* @ngInject */
    function TransferReceivedController(modalDialog, $timeout, userService, transferredFiles) {
        var vm = this;
        vm.transferTooltipEnter = transferTooltipEnter;
        vm.transferTooltipLeave = transferTooltipLeave;
        vm.enjoyPro = enjoyPro;
        vm.selectFunction = selectFunction;
        vm.selectBackground = selectBackground;
        vm.bytesToHumanFormat = bytesToHumanFormat;
        vm.toggleFileMenu = toggleFileMenu;
        vm.getHelp = getHelp;
        vm.deleteFile = deleteFile;
        vm.share = share;
        vm.transferToMobile = transferToMobile;

        activate();

        function enjoyPro() {
            modalDialog.enjoyPro().then(function () {
                modalDialog.gift();
            });
        }

        function transferTooltipEnter(item) {
            switch (item) {
                case 'link':
                    vm.title = 'Get download link';
                    break;
                case 'email':
                    vm.title = 'Transfer files by email';
                    break;
                case 'share':
                    vm.title = 'Sharing Options';
                    break;
                case 'mobile':
                    vm.title = 'Transfer to mobile';
                    break;
            }
        }

        function selectFunction(item) {
            vm.selectedFunction = item;
        }

        function transferTooltipLeave() {
            vm.title = vm.titleActive;
        }

        function activate() {
            vm.files = transferredFiles;
            calculateTotal();
            vm.title = 'How do you wish to share this transfer';
            vm.titleActive = vm.title;
            vm.selectedFunction = '';
            vm.email = '';

            $timeout(function () {
                vm.showTransferBox = true;
                $timeout(function () {
                    vm.showEnjoyProBox = true;
                }, 300)
            }, 200)
        }

        function selectBackground() {
            userService.checkPro().then(function (data) {
                if (!data) {
                    enjoyPro();
                    return;
                }

                modalDialog.setBackground();
            });
        }

        function bytesToHumanFormat(size, measure) {
            size *=  1.0;
            if (size < 1024 * 1024) {
                measure = 'Kb';
            }
            else if (size < 1024 * 1024 * 1024) {
                measure = 'Mb';
            }
            else {
                measure = 'Gb';
            }

            var pow = 0;
            switch (measure) {
                case 'Kb':
                    pow = 1;
                    break;
                case 'Mb':
                    pow = 2;
                    break;
                case 'Gb':
                    pow = 3;
                    break;
            }

            return Math.round(size / Math.pow(1024, pow) * 100) / 100 + ' ' + measure;
        }

        function toggleFileMenu(file){
            file.opened = !file.opened;
        }

        function calculateTotal(){
            vm.totalSize = 0;

            for (var index = 0; index < vm.files.length; index++) {
                vm.totalSize += vm.files[index].size;
            }
        }

        function getHelp(){
            modalDialog.help();
        }

        function deleteFile(index) {
            modalDialog.deleteConfirmation().then(function(){
                vm.files.splice(index, 1);
                calculateTotal();
            });
        }

        function share(file){
            modalDialog.shareOptions();
        }

        function transferToMobile(file){
            modalDialog.transferToMobile();
        }
    }
})();
(function () {
    'use strict';

    HistoryController.$inject = ["modalDialog"];
    angular
        .module('airfile.history')
        .controller('HistoryController', HistoryController);

    /* @ngInject */
    function HistoryController(modalDialog) {
        var vm = this;
        vm.toggleFileMenu = toggleFileMenu;
        vm.deleteFile = deleteFile;
        vm.share = share;
        vm.transferToMobile = transferToMobile;

        activate();

        function activate() {
            vm.spaceUsed = Math.round(Math.random() * 100) / 100;
            vm.files = [
                {name: 'some-file.jpg', date: '2016-10-03'},
                {name: 'blabla.png', date: '2016-10-03'},
                {name: 'owi-bla.jpg', date: '2016-10-02'}
            ];
        }

        function toggleFileMenu(file) {
            file.opened = !file.opened;
        }

        function deleteFile(index) {
            modalDialog.deleteConfirmation().then(function(){
                vm.files.splice(index, 1);
            });
        }

        function share(file){
            modalDialog.shareOptions();
        }

        function transferToMobile(file){
            modalDialog.transferToMobile();
        }
    }
})();
(function () {
    'use strict';

    configureStates.$inject = ["$stateProvider"];
    angular
        .module('airfile.history')
        .config(configureStates);

    /* @ngInject */
    function configureStates($stateProvider) {
        $stateProvider
            .state('history', {
                url: '/history',
                title: 'History',
              data: {
                  bodyClass: 'bg-airfile-white'
              },
                templateUrl: 'app/history/history.html',
                controller: 'HistoryController',
                controllerAs: 'vm'
            });
    }
})();
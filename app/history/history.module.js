(function () {
    'use strict';

    angular
        .module('airfile.history', [
            'airfile.layout'
        ]);
})();
(function () {
    'use strict';

    angular
        .module('airfile.landing')
        .directive('airFileCloud', cloud);

    /* @ngInject */
    function cloud() {
        var directive = {
            link: link,
            scope: {
                speed: '='
            },
            restrict: 'EA'
        };

        return directive;

        function link(scope, element, attrs) {
            function cloudAnim(cloud, speed) {
                function runInt() {
                    cloud.animate({'left': '+=100'}, {
                        easing: 'linear',
                        duration: speed,
                        complete: function() {
                            if ($(this).offset().left > $(window).width()) {
                                $(this).css('left', -20 + '%');
                            }
                            runInt();
                        }
                    });
                }

                runInt();
            }

            cloudAnim($(element), scope.speed || 7000);
        }
    }
})();
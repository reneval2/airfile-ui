(function () {
    'use strict';

    LandingController.$inject = ["modalDialog", "$state"];
    angular
        .module('airfile.landing')
        .controller('LandingController', LandingController);

    /* @ngInject */
    function LandingController(modalDialog, $state) {
        var vm = this;
        vm.enjoyPro = enjoyPro;
        vm.goToTransfer = goToTransfer;

        function enjoyPro(){
            modalDialog.enjoyPro().then(function(){
                modalDialog.gift();
            });
        }

        function goToTransfer(){
            $state.go("transfer","","{reload: true}");
        }
    }
})();
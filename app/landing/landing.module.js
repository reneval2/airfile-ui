(function () {
    'use strict';

    angular
        .module('airfile.landing', [
            'airfile.common'
        ]);
})();
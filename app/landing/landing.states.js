(function () {
    'use strict';

    configureStates.$inject = ["$stateProvider"];
    angular
        .module('airfile.landing')
        .config(configureStates);

    /* @ngInject */
    function configureStates($stateProvider) {
        $stateProvider
            .state('landing', {
                url: '/',
                title: 'Landing page',
                templateUrl: 'app/landing/landing.html',
                controller: 'LandingController',
                controllerAs: 'vm'
            });
    }
})();
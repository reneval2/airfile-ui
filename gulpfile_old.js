'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var gutil = require('gulp-load-plugins')({lazy: true});
var del = require('del');
var browserSync = require('browser-sync');


gulp.task('default', ['dev-build']);

gulp.task('build', ['optimize', 'copy-images', 'copy-fonts', 'clean']);

gulp.task('optimize', ['dev-build', 'cache-templates'], function () {
    log('Optimizing javascript, css and templates in html files');

    return gulp
        .src(config.index)
        .pipe(gutil.plumber())
        .pipe(inject(config.temp + config.templateCache.file, 'templates'))
        .pipe(gutil.useref())
        //.pipe(gutil.if('*.html', gutil.htmlmin({collapseWhitespace: true})))
        //.pipe(gutil.if('*.js', gutil.uglify()))
        //.pipe(gutil.if('*.css', gutil.minifyCss()))
        .pipe(gulp.dest(config.buildContent));
});

gulp.task('clean', ['optimize'], function () {
    log('Removing temp files');
    return del(config.temp);
});

gulp.task('cache-templates', function () {
    return gulp
        .src(config.appTemplates)
        .pipe(gutil.plumber())
        //.pipe(gutil.if('*.html', gutil.htmlmin()))
        .pipe(gutil.angularTemplatecache(config.templateCache.file, config.templateCache.options))
        .pipe(gulp.dest(config.temp));
});

// start webserver from 'public' folder to check how it will look in production
gulp.task('server-build', function (done) {

    return browserSync({
        server: {
            baseDir: '../public/'
        }
    }, done);
});

gulp.task('dev-build', ['inject', 'annotate', 'sass', 'fonts']);

gulp.task('inject', ['inject-app']);

gulp.task('copy-images', function () {
    return gulp
        .src(config.images, {"base": config.content})
        .pipe(gulp.dest(config.buildContent));
});

gulp.task('copy-fonts', ['dev-build'], function () {
    return gulp
        .src(config.publishedFonts, {"base": config.content})
        .pipe(gulp.dest(config.buildContent));
});

gulp.task('inject-app',  function () {
    return gulp
        .src(config.index, {base: config.root})
        .pipe(inject(config.appjs, 'app', config.jsOrder))
        .pipe(gulp.dest(config.root));
});

gulp.task('bower', function () {
    log('Wire up the bower js index.html');

    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;

    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe(gulp.dest(config.root));
});

gulp.task('annotate', function () {
    return gulp.src(config.appjs)
        .pipe(gutil.ngAnnotate({remove: true, add: true}))
        .pipe(gulp.dest(config.app));
});

gulp.task('sass', function () {
    var sass = require('gulp-sass');
    return gulp.src(config.scss)
        .pipe(sass())
        .pipe(gulp.dest(config.cssFolder));
});

gulp.task('fonts-boostrap', function () {
    return copyFonts('bootstrap', './content/fonts');
});

gulp.task('fonts-font-awesome', function () {
    return copyFonts('fontAwesome', './content/fonts');
});

gulp.task('fonts-roboto', function () {
    return copyFonts('roboto', './content/fonts');
});

gulp.task('fonts-neo-sans', function () {
    return copyFonts('neoSans', './content/fonts');
});

gulp.task('fonts', ['fonts-boostrap', 'fonts-font-awesome', 'fonts-roboto', 'fonts-neo-sans']);

gulp.task('nodemon', ['default'], function () {
    var nodemon = require('gulp-nodemon');
    nodemon({
        script: 'server.js'
        , ext: 'js html'
        , env: { 'NODE_ENV': 'development' }
    })
});

gulp.task('browser-sync', ['nodemon'], function() {
    var browserSync = require('browser-sync').create();
    browserSync.init(null, {
        proxy: "http://localhost:8080", // port of node server
    });
});



function log(msg) {
    if (typeof msg === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                gutil.util.log(gutil.util.colors.bgBlue(msg[item]));
            }
        }
    } else {
        gutil.util.log(gutil.util.colors.bgBlue(msg));
    }
}

function inject(src, label, order) {
    var options = {addRootSlash: false};
    if (label) {
        options.name = 'inject:' + label;
    }

    return gutil.inject(orderSrc(src, order), options);
}

function orderSrc(src, order) {
    return gulp
        .src(src, {read: false})
        .pipe(gutil.if(order, gutil.order(order)));
}

function copyFonts(packageName, path) {
    log('Copying ' + packageName + ' fonts');

    return gulp
        .src(config.fonts[packageName])
        .pipe(gulp.dest(path));
}
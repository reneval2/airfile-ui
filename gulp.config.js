'use strict';

module.exports = function () {
    var app = './app/';
    var config = {
        /**
         * File paths
         */
        root: './',
        build: '../public/',
        buildContent: '../public/content',
        temp: './tmp/',
        app: app,
        appTemplates: app + '**/*.html',
        index: './index.html',
        // All custom CSS files
        scss: './content/scss/**/*.scss',
        content: './content',
        publishedFonts: './content/fonts/**/*',
        images: './content/img/**/*',
        cssFolder: './content/css/',
        // JavaScript files order
        jsOrder: [
            '**/app.module.js',
            '**/*.module.js',
            '**/*.js'
        ],
        appjs: [
            app + '**/*.js'
        ],
        bower: {
            json: require('./bower.json'),
            directory: './bower_components',
            ignorePath: '../..',
            exclude: ['bower_components/roboto-fontface']
        },
        fonts: {
            bootstrap: './bower_components/bootstrap/fonts/**',
            fontAwesome: './bower_components/font-awesome/fonts/**',
            roboto: './bower_components/roboto-fontface/fonts/Roboto-Regular.{eot,svg,ttf,woff,woff2}',
            neoSans: './content/additional-fonts/neo-sans/**'
        },
        templateCache: {
            file: 'templates.js',
            options: {
                module: 'airfile.common',
                root: 'app/',
                standAlone: false
            }
        },
        getWiredepDefaultOptions: getWiredepDefaultOptions
    };

    function getWiredepDefaultOptions() {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath,
            exclude: config.bower.exclude,
            "overrides": {
                "angular-bootstrap": {
                    "main": [
                        "ui-bootstrap.js",
                        "ui-bootstrap-tpls.js"
                    ]
                },
                "bootstrap": {
                    "main": [
                        "./dist/css/bootstrap.css"
                    ]
                },
                "font-awesome": {
                    "main": [
                        "./css/font-awesome.css"
                    ]
                }
            }
        };

        return options;
    }

    return config;
};
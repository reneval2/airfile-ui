'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'gulp.*','plumber*'],
  camelize: true,
  lazy:false
});
var runSequence = require('run-sequence');


var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var browserSync = require('browser-sync');
var reload = browserSync.reload;



gulp.task('default', ['nodemon']);

gulp.task('run-dev', ['browser-sync'], function () {
    gulp.watch('styles/*.css', function (file) {
        if (file.type === "changed") {
            reload(file.path);
        }
    });
    gulp.watch(['*.html', 'app/**/*.html'], ['bs-reload']);
    gulp.watch(['app/**/*.js'], ['bs-reload']);
    gulp.watch('content/**/*.scss', ['sass', 'minify-css']);
});

// delete build folder
gulp.task('clean:build', function (cb) {
  del([
    '../public'
  ], cb);
});


gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});

// SASS task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('sass', function () {
  return gulp.src('content/scss/style.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      style: 'expanded'
    }))
    .on('error', $.notify.onError({
      title: 'SASS Failed',
      message: 'Error(s) occurred during compile!'
    }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('content/css'))
    .pipe(reload({
      stream: true
    }))
    .pipe($.notify({
      message: 'Styles task complete'
    }));
});

// SASS Build task
gulp.task('sass:build', function () {
  var s = $.size();

  return gulp.src('content/scss/style.scss')
    .pipe($.sass({
      style: 'compact'
    }))
    .pipe($.autoprefixer('last 3 version'))
    .pipe($.uncss({
      html: ['./index.html' , './app/**/*.html'],
      ignore: [
        '.index',
        '.slick',
        /\.owl+/,
        /\.owl-next/,
        /\.owl-prev/
      ]
    }))
    .pipe($.minifyCss({
      keepBreaks: true,
      aggressiveMerging: false,
      advanced: false
    }))
    .pipe($.rename({suffix: '.min'}))
    .pipe(gulp.dest('../public/content/css'))
    .pipe(s)
    .pipe($.notify({
      onLast: true,
      message: function () {
        return 'Total CSS size ' + s.prettySize;
      }
    }));
});
// minify CSS
gulp.task('minify-css', function () {
    gulp.src(['./content/**/*.css', '!./styles/**/*.min.css'])
      .pipe($.rename({suffix: '.min'}))
      .pipe($.minifyCss({keepBreaks: true}))
      .pipe(gulp.dest('./styles/'))
      .pipe(gulp.dest('../public/content/css/'));
});

// BUGFIX: warning: possible EventEmitter memory leak detected. 11 listeners added.
require('events').EventEmitter.prototype._maxListeners = 100;


gulp.task('usemin', function () {
  return gulp.src('./index.html')
    // add templates path
    .pipe($.htmlReplace({

      'templates': {src: null,
        tpl:'<script type="text/javascript" src="./scripts/templates.js"></script>'}
    }))
    .pipe($.usemin({
      css: [$.minifyCss(), 'concat'],
      libs: [$.uglify()],
      nonangularlibs: [$.uglify()],
      angularlibs: [$.uglify()],
      appcomponents: [$.uglify()],
      mainapp: [$.uglify()]
    }))
    .pipe(gulp.dest('../public'));
});

// calculate build folder size
gulp.task('build:size', function () {
  var s = $.size();

  return gulp.src('../public/**/*.*')
    .pipe(s)
    .pipe($.notify({
      onLast: true,
      message: function () {
        return 'Total build size ' + s.prettySize;
      }
    }));
});

// start webserver from 'public' folder to check how it will look in production
gulp.task('server-build', function (done) {
    return browserSync({
        server: {
            baseDir: '../public/'
        }
    }, done);
});

// reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('cache-templates', function () {
    return gulp
        .src(config.appTemplates)
        .pipe($.plumber())
        //.pipe($.if('*.html', $.htmlmin()))
        .pipe($.angularTemplatecache(config.templateCache.file, config.templateCache.options))
        .pipe(gulp.dest("../public/scripts"));
});


gulp.task('copy-images', function () {
    return gulp
        .src(config.images, {"base": config.content})
        .pipe(gulp.dest(config.buildContent));
});

gulp.task('copy-fonts', function () {
    return gulp
        .src(config.publishedFonts, {"base": config.content})
        .pipe(gulp.dest(config.buildContent));
});

gulp.task('nodemon',  function () {
    var nodemon = require('gulp-nodemon');
    nodemon({
        script: 'server.js'
        , ext: 'js html'
        , env: { 'NODE_ENV': 'development' }
    })
});

gulp.task('build', function (callback) {
  runSequence(
    //'clean:build',
    'sass',
    'sass:build',
    'copy-images',
    'cache-templates',
    'usemin',
    'copy-fonts',
    'build:size',
    callback);
});
